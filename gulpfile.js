// Packages
// Gulp packages are loaded through gulp-load-plugins
const { src, dest, watch, series, parallel } = require('gulp')
const browserSync = require('browser-sync')
const through = require('through2')
const rimraf = require('rimraf')
const gulpLoadPlugins = require('gulp-load-plugins')
const PLUGIN = gulpLoadPlugins({
  rename: {
    'gulp-rev-css-url': 'override',
    'gulp-rev-outdated': 'revOutdated'
  }
})

// Path to local url
const LOCAL_URL = './'

// Set path to Foundation files
const FOUNDATION = 'node_modules/foundation-sites';

// Paths to files
const SOURCE = {
  // Js files to be minified
  scripts: [

    // Foundation core - needed if you want to use any of the components below
		FOUNDATION + '/dist/js/plugins/foundation.core.js',
		FOUNDATION + '/dist/js/plugins/foundation.util.*.js',

		// Pick the components you need in your project
		FOUNDATION + '/dist/js/plugins/foundation.abide.js',
		FOUNDATION + '/dist/js/plugins/foundation.accordion.js',
		FOUNDATION + '/dist/js/plugins/foundation.accordionMenu.js',
		FOUNDATION + '/dist/js/plugins/foundation.drilldown.js',
		FOUNDATION + '/dist/js/plugins/foundation.dropdown.js',
		FOUNDATION + '/dist/js/plugins/foundation.dropdownMenu.js',
		FOUNDATION + '/dist/js/plugins/foundation.equalizer.js',
		// FOUNDATION + '/dist/js/plugins/foundation.interchange.js',
		FOUNDATION + '/dist/js/plugins/foundation.offcanvas.js',
		// FOUNDATION + '/dist/js/plugins/foundation.orbit.js',
		FOUNDATION + '/dist/js/plugins/foundation.responsiveMenu.js',
		FOUNDATION + '/dist/js/plugins/foundation.responsiveToggle.js',
		FOUNDATION + '/dist/js/plugins/foundation.reveal.js',
		// FOUNDATION + '/dist/js/plugins/foundation.slider.js',
		FOUNDATION + '/dist/js/plugins/foundation.smoothScroll.js',
		// FOUNDATION + '/dist/js/plugins/foundation.magellan.js',
		FOUNDATION + '/dist/js/plugins/foundation.sticky.js',
		FOUNDATION + '/dist/js/plugins/foundation.tabs.js',
		// FOUNDATION + '/dist/js/plugins/foundation.responsiveAccordionTabs.js',
		FOUNDATION + '/dist/js/plugins/foundation.toggler.js',
    FOUNDATION + '/dist/js/plugins/foundation.tooltip.js',
    
    // For custom js
    'assets/scripts/js/**/*.js',
  ],

  // Scss files will be concantonated, minified if ran with --production
  styles: 'assets/styles/scss/**/*.scss',

  // Images placed here will be optimized
  images: 'assets/img/src/**/*',
}

const ASSETS = {
  styles: 'assets/styles',
  scripts: 'assets/scripts',
  images: 'assets/img',
  root: 'assets/'
}

// Concat and minify scripts
function scripts () {
  return src(SOURCE.scripts)
    .pipe(PLUGIN.sourcemaps.init())
    .pipe(PLUGIN.concat('main.js'))
    .pipe(PLUGIN.terser())
    .pipe(PLUGIN.sourcemaps.write('.'))
    .pipe(dest(ASSETS.scripts))
}
exports.scripts = scripts


// Compile scss, autoprefix and minify
function styles () {
  return src(SOURCE.styles)
    .pipe(PLUGIN.sourcemaps.init())
    .pipe(PLUGIN.sass().on('error', PLUGIN.sass.logError))
    .pipe(PLUGIN.autoprefixer({
      browsers: [
        'last 2 versions',
        'ie >= 9',
        'ios >= 7'
      ],
      cascade: false
    }))
    .pipe(PLUGIN.cssnano())
    .pipe(PLUGIN.sourcemaps.write('.'))
    .pipe(dest(ASSETS.styles))
    .pipe(browserSync.stream())
}
exports.styles = styles

// Optimize images
function images () {
  return src(SOURCE.images)
    .pipe(PLUGIN.imagemin())
    .pipe(dest(ASSETS.images))
}
exports.images = images

// Browser-Sync watch files and inject changes
function browsersync () {
  // Watch these files for changes
  let files = [
    '**/*.html'
  ]

  browserSync.init(files, {
    server: {
      baseDir: LOCAL_URL
    }
  })

  watch(SOURCE.styles, parallel(styles))
  watch(SOURCE.scripts, parallel(scripts)).on('change', browserSync.reload)
  watch(SOURCE.images, parallel(images)).on('change', browserSync.reload)
}
exports.browsersync = browsersync