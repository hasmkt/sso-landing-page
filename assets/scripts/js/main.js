jQuery(document).foundation();

jQuery(document).ready(function($) {
  // Get url params and pre-fill form
  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if(pair[0] == variable){return pair[1];}
    }
    return(false);
  }
  
  // Put query params into object
  let contactDetails = {
    firstName: getQueryVariable('firstName'),
    lastName : getQueryVariable('lastName'),
    email    : getQueryVariable('email')
  }

  // Call object if the value isn't false
  if (contactDetails.firstName != false) {
    $('#amf-input-first_name_925').val(contactDetails.firstName);
  }
  
  if (contactDetails.lastName != false) {
    $('#amf-input-last_name_926').val(contactDetails.lastName);
  }

  if (contactDetails.email != false) {
    $('#amf-input-email_address_927').val(contactDetails.email);
  }

});
